import { Dish } from './dish.model';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, tap } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Injectable()
export class DishService{
  dishesChange = new Subject<Dish[]>();
  dishesFetching = new Subject<boolean>();
  dishUpLoading = new Subject<boolean>();
  constructor(private http: HttpClient) {
  }
  private dishes : Dish[] = [];
  getDishes(){
    return this.dishes.slice();
  }
  getDish(index: number){
    return this.dishes[index]
  }
  addDish(dish: Dish ){
    const body = {
      name: dish.name,
      description: dish.description,
      imageUrl: dish.imageUrl,
      price: dish.price
    };
    this.dishUpLoading.next(true)
    return this.http.post('https://plovo-bb6ec-default-rtdb.firebaseio.com/dishes.json', body)
      .pipe(
        tap(() => {
          this.dishUpLoading.next(false)
        }, () => {
          this.dishUpLoading.next(false)
    }
        )
      )
  }
  fetchDish(id: string){
    return this.http.get<Dish>(`https://plovo-bb6ec-default-rtdb.firebaseio.com/dishes/${id}.json`)
      .pipe(map(result => {
        return new Dish(
          id, result.name, result.description,
          result.imageUrl, result.price
        );
      })
      )
  }
  fetchDishes(){
    this.dishesFetching.next(true)
    this.http.get<{[id: string]:Dish}>
    ('https://plovo-bb6ec-default-rtdb.firebaseio.com/dishes.json')
      .pipe(map(result => {
        return Object.keys(result).map(id => {
          const dishData = result[id]
          return new Dish (
            id, dishData.name, dishData.description,
            dishData.imageUrl, dishData.price
          )
        })
      }))
      .subscribe(dishes => {
      this.dishes = dishes;
      this.dishesChange.next(this.dishes.slice());
      this.dishesFetching.next(false)
    }, error => {
      this.dishesFetching.next(false)
    })
  }
}
