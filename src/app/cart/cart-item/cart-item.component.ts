import { Component, Input } from '@angular/core';
import { CartItem } from '../../shared/cart-item';

@Component({
  selector: 'app-cart-item',
  templateUrl: './cart-item.component.html',
  styleUrls: ['./cart-item.component.css']
})
export class CartItemComponent{
  @Input() cartItem!: CartItem;



}
