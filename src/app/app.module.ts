import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { NewDishComponent } from './new-dish/new-dish.component';
import { DishesComponent } from './dishes/dishes.component';
import { DishItemComponent } from './dishes/dish-item/dish-item.component';
import { CartComponent } from './cart/cart.component';
import { CartItemComponent } from './cart/cart-item/cart-item.component';
import { FormsModule } from "@angular/forms";
import { DishService } from './shared/dish.services';
import { CartService } from './shared/cart.service';
import { ModalComponent } from './ui/modal/modal.component';
import { ShadowHoverDirective } from './directives/shadow.hover.directives';
import { HomeComponent } from './home/home.component';
import { NotFoundComponent } from './not-found';
import { ManageDishesComponent } from './dishes/manage-dishes/manage-dishes.component';
import { DishDetailsComponent } from './dishes/manage-dishes/dish-details/dish-details.component';
import { EmptyDishComponent } from './dishes/manage-dishes/empty-dish.component';
import { AppRoutingModule } from './app-routing-module';
import { HttpClientModule } from '@angular/common/http';




@NgModule({
  declarations: [
    AppComponent,
    ToolbarComponent,
    NewDishComponent,
    DishesComponent,
    DishItemComponent,
    CartComponent,
    CartItemComponent,
    ModalComponent,
    ShadowHoverDirective,
    HomeComponent,
    NotFoundComponent,
    ManageDishesComponent,
    DishDetailsComponent,
    EmptyDishComponent

  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [DishService, CartService],
  bootstrap: [AppComponent]
})
export class AppModule { }
