import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Dish } from '../shared/dish.model';
import { DishService } from '../shared/dish.services';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-new-dish',
  templateUrl: './new-dish.component.html',
  styleUrls: ['./new-dish.component.css'],
})
export class NewDishComponent implements OnInit, OnDestroy{
  @ViewChild('nameInput') nameInput!: ElementRef;
  @ViewChild('descriptionInput') descriptionInput!: ElementRef;
  @ViewChild('imageUrlInput') imageUrlInput!: ElementRef;
  @ViewChild('priceInput') priceInput!: ElementRef;
  isUpLoading = false;
  dishUpLoadingSubscription!: Subscription;
  constructor(private dishService: DishService,
              private router: Router,
              private route: ActivatedRoute
              ) {
  }
  ngOnInit(){
    this.dishUpLoadingSubscription = this.dishService.dishUpLoading.subscribe((isUpLoading: boolean) => {
      this.isUpLoading = isUpLoading;
    });
  }
  createDish() {
    const name = this.nameInput.nativeElement.value;
    const description = this.descriptionInput.nativeElement.value;
    const imageUrl = this.imageUrlInput.nativeElement.value;
    const price = parseFloat(this.priceInput.nativeElement.value);
    const id = Math.random().toString()
    // const body = {}
    // this.http.post('https://plovo-bb6ec-default-rtdb.firebaseio.com/dishes.json', body).subscribe()
     const dish = new Dish(id, name, description, imageUrl, price);
    this.dishService.addDish(dish).subscribe(() => {
      this.dishService.fetchDishes();
      void  this.router.navigate(['..'], {relativeTo: this.route})
    });
  }
  ngOnDestroy() {
    this.dishUpLoadingSubscription.unsubscribe()
  }
}
