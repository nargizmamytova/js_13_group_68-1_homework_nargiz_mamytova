import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { DishService } from '../../../shared/dish.services';
import { Dish } from '../../../shared/dish.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-dish-details',
  templateUrl: './dish-details.component.html',
  styleUrls: ['./dish-details.component.css']
})
export class DishDetailsComponent implements OnInit, OnDestroy{
  dish!: Dish;
  subscription!: Subscription;
  constructor(
    private route: ActivatedRoute,
    private dishService: DishService
  ) { }

  ngOnInit(): void {
    this.route.data.subscribe(data => {
      this.dish = <Dish>data.dish;
    })
  }
  ngOnDestroy(){
    this.subscription.unsubscribe()
  }
}

